//
//  ViewController.swift
//  DocsApp
//
//  Created by Apple on 22/02/16.
//  Copyright © 2016 jonty. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var sendButton: UIButton!
    let appDelegate =
    UIApplication.sharedApplication().delegate as! AppDelegate
    
    var chat = [NSManagedObject]()
    
    var inputTextField: UITextField?
    
    @IBOutlet weak var chatTableVIew: UITableView!
    
    @IBOutlet weak var chatField: UITextField!
    var chatArray = [String]()
    var usernameArray = [String]()
    
    var chatCount:Int = 0
    
    var username = "chirag1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let actionSheet = UIAlertController(title: "Enter Username", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        actionSheet.addTextFieldWithConfigurationHandler { (textField) -> Void in
            self.inputTextField = textField
        }
        
        let saveAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if self.inputTextField?.text?.stringByReplacingOccurrencesOfString(" ", withString: "") != "" {
                self.username = (self.inputTextField?.text)!
            }
            else {
                self.username = "Guest"
            }
            
        })
        actionSheet.addAction(saveAction)
        self.presentViewController(actionSheet, animated: true, completion: nil)
        
        chatTableVIew.rowHeight = UITableViewAutomaticDimension
        chatTableVIew.estimatedRowHeight = 75
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewWillAppear(animated: Bool) {
         self.fetchData()
    }
    
    // MARK: - TableView Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return chatCount
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell  {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! ChatCell
        if usernameArray[indexPath.row] == self.username {
            cell.messageTextLabel.textAlignment = .Left
            cell.receiverNameWidthConstraint.constant = 0
            cell.senderNameWidthConstraint.constant = 35
            let firstLetterUserName =  "\(usernameArray[indexPath.row].characters.first!)"
            cell.senderNameInitialLabel.text = firstLetterUserName.uppercaseString
        }else {
            cell.messageTextLabel.textAlignment = .Right
            cell.senderNameWidthConstraint.constant = 0
            cell.receiverNameWidthConstraint.constant = 35
            let firstLetterUserName =  "\(usernameArray[indexPath.row].characters.first!)"
            cell.receiverNameInitialLabel.text = firstLetterUserName.uppercaseString
        }
        
        cell.messageTextLabel.text = chatArray[indexPath.row]
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
        
    }
    
    //MARK:- Send Message
    @IBAction func sendMessage(sender: AnyObject) {
        if IJReachability.isConnectedToNetwork(){
            if chatField.text?.characters.count != 0 || chatField.text != "" {
                inputTextField?.enabled = false
                activityIndicator.startAnimating()
                sendButton.hidden = true
                var urlString = "http://www.personalityforge.com/api/chat/?apiKey=6nt5d1nJHkqbkphe&message=\(chatField.text!)&chatBotID=63906&externalID=\(username)"
                
                urlString = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
                
                request(.GET, urlString, parameters: nil).responseJSON(completionHandler: { (response) -> Void in
                    if let json = response.result.value as? NSDictionary {
                        if let messageDictionary = json["message"] as? NSDictionary {
                            if let message = messageDictionary["message"] as? String {
                                self.insertMessage(self.chatField.text!, uname: self.username)
                                self.insertMessage(message, uname: "Bot")
                                self.appDelegate.saveContext()
                                self.fetchData()
                                
                            }
                        }
                    }
                    
                    self.inputTextField?.text = ""
                    self.inputTextField?.enabled = true
                    self.activityIndicator.stopAnimating()
                    self.sendButton.hidden = false
                })
            }
        }
            else{
                self.showAlert("You may not have internet connection. Please check your connection and try again.")
            }
        
    }
   
    func showAlert (message:String) {
        let actionSheet = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        actionSheet.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    //MARK:- Caching Message
    func insertMessage (message:String, uname:String) {
        
        let entityDescription = NSEntityDescription.entityForName("Chat", inManagedObjectContext: appDelegate.managedObjectContext)
        let newPerson = NSManagedObject(entity: entityDescription!, insertIntoManagedObjectContext: appDelegate.managedObjectContext) as! Chat
        
        newPerson.username = uname
        newPerson.message = message
        newPerson.time = NSDate()
    }
    
    //MARK:- Fecth Messages
    func fetchData () {
        let fetchRequest = NSFetchRequest(entityName: "Chat")
        
        let sortDescriptor = NSSortDescriptor(key: "time", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        //3
        do {
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            chat = results as! [NSManagedObject]
            
            chatArray.removeAll()
            usernameArray.removeAll()
            for item in chat {
                chatArray.append((item as! Chat).message!)
                usernameArray.append((item as! Chat).username!)
            }
            
            chatCount = chatArray.count
            chatTableVIew.reloadData()
            if chatCount > 0 {
                let indexPath = NSIndexPath(forRow: chatCount-1, inSection:0)
                chatTableVIew.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK:- TableView Cell
class ChatCell: UITableViewCell {
    @IBOutlet weak var messageTextLabel: UILabel!
    @IBOutlet weak var senderNameInitialLabel: UILabel!
    @IBOutlet weak var receiverNameInitialLabel: UILabel!
    @IBOutlet weak var senderNameWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var receiverNameWidthConstraint: NSLayoutConstraint!
}